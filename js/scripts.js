// Калькулятор


let Calc = function(){
    this.activation = () => {
        this.on = confirm('Нажмите ОК для запуска калькулятора, иначе - Отмена');
        if(this.on === true) {
            this.getOption();
        } else {
            alert('Калькулятор не запущен')
        }
    }
    this.getOption = () => {
        this.mode = prompt('Введите "1" для выбора режима "вычисление выражения" или "2" для выбора режима "числа + операция"');
        if(this.mode === '1') {
            this.get1();
        } else if(this.mode === '2') {
            this.get2();
        } else {
            this.getOption();
        }
    };
    this.get1 = () =>{
        this.exp = prompt('Введите численное выражение(например: a + b)');
        this.oper = this.exp[1];
        this.a = +this.exp[0];
        this.b = +this.exp[2];
        this.operation();
    }
    this.get2 = () => {
        this.a = +prompt('Введите число а');
        this.b = +prompt('Введите число b');
        this.oper = prompt('Введите операцию(+, -, *, /');
        this.operation();
    };
    this.operation = () => {
        switch(this.oper) {
            case '+':
                this.result = this.a + this.b;
            break;
            case '-':
                this.result = this.a - this.b;
            break;
            case '*':
                this.result = this.a * this.b;
            break;
            case '/':
                this.result = this.a / this.b;
            break;
            default: alert('Ошибка ввода');
            this.getOption()
        }
        this.show();
    }
    this.show = () => {
        alert(`${this.a} ${this.oper} ${this.b} = ${this.result}`)
        this.deactivation();
    }
    this.deactivation = () => {
        this.off = confirm('Желаете продолжить?');
        if(this.off === true) {
            this.getOption();
        } else {
            return alert('Калькулятор выключен')
        }
    }
};

let calc = new  Calc();
calc.activation();

